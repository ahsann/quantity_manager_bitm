<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="../asset/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      
   
      
      
      
      <section>
          <div class="container">
              
              <div class="row">
                  <form class="form-group form-inline custom" action="store.php" method="POST">
                      <label>Weekend</label>
                      <input class="form-control " type="text" name="week" placeholder="Enter week name"/>
                       <br>
                      <br>
                      <label>Date</label>
                       <input class="form-control " type="text" name="date" placeholder="Enter Date"/>
                       <br>
                      <br>
                      <label>Manager</label>
                      <input class="form-control " type="text" name="manager" placeholder="Enter manager name"/>
                       <br>
                      <br>
                      <label>Qty</label>
                      <input class="form-control " type="text" name="qty" placeholder="Enter Quantity"/>
                      <br>
                      <br>
                      <input class="form-control btn-primary" type="submit" value="SUBMIT" />
                      <input class="form-control btn-danger" type="reset" value="Reset" />
                      
                  </form>
              </div>
          </div>
      </section>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="asset/js/bootstrap.min.js"></script>
  </body>
</html>
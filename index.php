<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Bootstrap 101 Template</title>

        <!-- Bootstrap -->
        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="asset/css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        
    <?php
     ini_set('display_errors','Off');
     include_once './vendor/autoload.php';
     use App\table\Table;
     
     $obj = new Table();
     $quantity = $obj->index();
     $total = $obj->sum();
    ?>
        
        
        <section>
            <div class="container">
                
                <div class="row">
                    <a class="btn btn-success" href="view/create.php">Create new</a><br/><hr/>
                    <table style="width: 350px">


                        <thead>
                            <tr>
                                <th>Weekday</th>
                                <th>Date</th>
                                <th>Manager</th>
                                <th>Qty</th>
                            </tr>
                        </thead>
                        <tbody >
                            <?php 
                                foreach ($quantity as $element) {
                            ?>
                            <tr>
                                <th><?php echo $element->day ?></th>
                                <td><?php echo date('d-m-Y', strtotime($element->date));?></td>
                                <td><?php echo $element->manager ?></td>
                                <td><?php echo $element->quantity ?></td>
                            </tr>
                            <?php 
                                }
                            ?>
                        </tbody>
                        <tfoot>
                        <th colspan="3">Total</th>

                        <th><?php echo $total ?></th>
                        </tfoot>
                    </table>
                </div>
            </div>
        </section>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="asset/js/bootstrap.min.js"></script>
    </body>
</html>